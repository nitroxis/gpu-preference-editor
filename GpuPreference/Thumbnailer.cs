﻿using System;
using System.IO;
using System.Threading.Channels;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Microsoft.WindowsAPICodePack.Shell;

namespace GpuPreference;

public record ThumbRequest(string Path, Action<BitmapSource> Callback);

public static class Thumbnailer
{
	private static Task? thumbTask;
	private static ChannelWriter<ThumbRequest>? thumbChannel;

	public static bool RequestThumbs(ThumbRequest req)
	{
		return thumbChannel?.TryWrite(req) ?? false;
	}

	public static async Task Start()
	{
		await Stop();
		Channel<ThumbRequest> channel = Channel.CreateUnbounded<ThumbRequest>();
		thumbChannel = channel.Writer;
		thumbTask = thumbsProcessor(channel.Reader);
	}

	public static async Task Stop()
	{
		thumbChannel?.Complete();

		if (thumbTask != null)
			await thumbTask;
	}

	private static async Task thumbsProcessor(ChannelReader<ThumbRequest> reader)
	{
		await foreach (ThumbRequest req in reader.ReadAllAsync())
		{
			string realPath = Path.GetFullPath(req.Path);
			if (!File.Exists(realPath))
				continue;

			ShellFile? file = ShellFile.FromFilePath(realPath);
			req.Callback(file.Thumbnail.SmallBitmapSource);
		}
	}
}