﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Microsoft.Win32;
using NvAPIWrapper.DRS;
using NvAPIWrapper.GPU;

namespace GpuPreference;

public class APIs
{
	public static RegistryKey UserGpuPreferences { get; }

	public static DriverSettingsSession Drs { get; }

	public static ObservableCollection<DrsGpu> DrsGpus { get; }

	static APIs()
	{
		UserGpuPreferences = Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\DirectX\UserGpuPreferences", true) ?? throw new KeyNotFoundException(@"Key HKCU\Software\Microsoft\DirectX\UserGpuPreferences not found");

		Drs = DriverSettingsSession.CreateAndLoad();
		SettingInfo glSetting = SettingInfo.FromId((uint)KnownSettingId.OpenGLImplicitGPUAffinity)!;

		string[] ids = glSetting.AvailableValues.OfType<string>().ToArray();

		DrsGpus = new ObservableCollection<DrsGpu>();
		PhysicalGPU[]? gpus = PhysicalGPU.GetPhysicalGPUs();

		foreach (string id in ids)
		{
			PhysicalGPU? gpu = gpus.FirstOrDefault(g =>
			{
				string s = $"{g.BusInformation.PCIIdentifiers.DeviceId:X8},{g.GPUId:X8}";
				return id.Contains(s);
			});

			if (gpu != null)
			{
				DrsGpus.Add(new DrsGpu(ShortenGpuName(gpu.FullName), id));
			}
			else
			{
				DrsGpus.Add(new DrsGpu(id, id));
			}
		}
	}

	public static string ShortenGpuName(string name)
	{
		return name
			.Replace("NVIDIA", "", StringComparison.InvariantCultureIgnoreCase)
			.Replace("GeForce", "", StringComparison.InvariantCultureIgnoreCase)
			.Trim();
	}
}