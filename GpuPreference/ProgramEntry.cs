﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using Microsoft.Win32;
using NvAPIWrapper.DRS;
using NvAPIWrapper.Native.Exceptions;
using NvAPIWrapper.Native.General;

namespace GpuPreference;

public class ProgramEntry : INotifyPropertyChanged
{
	private string path = "";
	private string? gpuInfo;

	private bool thumbRequested;
	private BitmapSource? thumb;

	public ProgramKind Kind { get; set; }

	public HashSet<int> ProcessIds { get; } = new();

	public string ProcessId => string.Join(", ", this.ProcessIds);

	public string Path
	{
		get => this.path;
		set
		{
			if (!this.SetField(ref this.path, value))
				return;

			this.thumbRequested = false;
			this.Thumb = null;

			object? oldValue = APIs.UserGpuPreferences.GetValue(this.path);
			if (oldValue is string oldStr)
			{
				APIs.UserGpuPreferences.DeleteValue(this.path);
				APIs.UserGpuPreferences.SetValue(value, oldStr, RegistryValueKind.String);
			}
			else if (oldValue != null)
			{
				throw new InvalidOperationException("Registry key is not a string");
			}
		}
	}

	public bool FileExists => this.Kind == ProgramKind.App || System.IO.File.Exists(this.Path);

	public Preference Preference
	{
		get
		{
			if (APIs.UserGpuPreferences.GetValue(this.Path) is not string regValue)
				return Preference.Unset;

			string[] split = regValue.Split(';', StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries);
			if (split.FirstOrDefault(s => s.StartsWith("GpuPreference=")) is not string gpuPref)
				return Preference.Unset;

			gpuPref = gpuPref[14..];
			if (int.TryParse(gpuPref, CultureInfo.InvariantCulture, out int n))
				return (Preference)n;

			return Preference.Unset;
		}
		set
		{
			if (APIs.UserGpuPreferences.GetValue(this.Path) is not string regValue)
				regValue = "";

			List<string> split = regValue.Split(';', StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries).ToList();

			split.RemoveAll(s => s.StartsWith("GpuPreference="));

			if (value >= 0)
			{
				split.Add($"GpuPreference={(int)value}");
			}

			if (split.Count == 0)
			{
				try
				{
					APIs.UserGpuPreferences.DeleteValue(this.Path);
				}
				catch (ArgumentException)
				{
				}
			}
			else
			{
				regValue = string.Join(";", split) + ";";
				APIs.UserGpuPreferences.SetValue(this.Path, regValue);
			}

			this.OnPropertyChanged();
		}
	}

	public BitmapSource? Thumb
	{
		get
		{
			if (!this.thumbRequested)
			{
				Dispatcher disp = Dispatcher.CurrentDispatcher;
				Thumbnailer.RequestThumbs(new(this.path, th => { disp.Invoke(() => { this.Thumb = th; }); }));
				this.thumbRequested = true;
			}

			return this.thumb;
		}
		set
		{
			if (!this.SetField(ref this.thumb, value))
				return;

			if (value != null)
				this.thumbRequested = true;
		}
	}

	public string? GpuInfo
	{
		get => this.gpuInfo;
		set => this.SetField(ref this.gpuInfo, value);
	}

	public DrsGpu? OpenGLGpu
	{
		get
		{
			DriverSettingsProfile? profile;
			try
			{
				profile = APIs.Drs.FindApplicationProfile(this.path);
			}
			catch (NVIDIAApiException ex) when (ex.Status == Status.ProfileNotFound)
			{
				profile = null;
			}

			ProfileSetting? setting = profile?.GetSetting(KnownSettingId.OpenGLImplicitGPUAffinity);
			string? id = setting?.CurrentValue as string;
			if (id == null)
				return null;

			return APIs.DrsGpus.FirstOrDefault(g => g.Id == id);
		}
		set
		{
			DriverSettingsProfile? profile;
			try
			{
				profile = APIs.Drs.FindApplicationProfile(this.path);
			}
			catch (NVIDIAApiException ex) when (ex.Status == Status.ProfileNotFound)
			{
				profile = null;
			}

			if (profile == null)
			{
				if (value != null)
				{
					profile = DriverSettingsProfile.CreateProfile(APIs.Drs, this.path);
					ProfileApplication.CreateApplication(profile, this.path, this.path);
				}
				else
				{
					this.OnPropertyChanged();
					return;
				}
			}

			if (value == null)
			{
				try
				{
					profile.DeleteSetting(KnownSettingId.OpenGLImplicitGPUAffinity);
				}
				catch (NVIDIAApiException ex) when (ex.Status == Status.SettingNotFound)
				{
				}
			}
			else
			{
				profile.SetSetting(KnownSettingId.OpenGLImplicitGPUAffinity, value?.Id);
			}

			APIs.Drs.Save();
			this.OnPropertyChanged();
		}
	}

	public string? OpenGLGpuName => this.OpenGLGpu?.Name;

	public event PropertyChangedEventHandler? PropertyChanged;

	public void OnPropertyChanged([CallerMemberName] string? propertyName = null)
	{
		this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
	}

	protected bool SetField<T>(ref T field, T value, [CallerMemberName] string? propertyName = null)
	{
		if (EqualityComparer<T>.Default.Equals(field, value))
			return false;
		field = value;
		this.OnPropertyChanged(propertyName);
		return true;
	}
}