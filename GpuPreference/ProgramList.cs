﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Core;
using Windows.Management.Deployment;
using NvAPIWrapper.GPU;
using NvAPIWrapper.Native;
using NvAPIWrapper.Native.GPU.Structures;

namespace GpuPreference;

public class ProgramList : ObservableCollection<ProgramEntry>
{
	public async Task LoadAsync()
	{
		this.Clear();

		// Registry entries.
		foreach (string name in APIs.UserGpuPreferences.GetValueNames())
		{
			if (name == "DirectXUserGlobalSettings")
				continue;

			this.Add(new ProgramEntry
			{
				Path = name
			});
		}

		// Store apps.
		PackageManager packageManager = new();
		foreach (Package package in packageManager.FindPackages())
		{
			foreach (AppListEntry appListEntry in package.GetAppListEntries())
			{
				string id = appListEntry.AppUserModelId;

				ProgramEntry? entry = this.FirstOrDefault(e => e.Path == id);
				if (entry == null)
				{
					this.Add(entry = new ProgramEntry
					{
						Path = id,
					});
				}

				BitmapImage? logo;
				try
				{
					logo = new BitmapImage(package.Logo);
				}
				catch (Exception)
				{
					logo = null;
				}

				entry.Kind = ProgramKind.App;
				entry.Thumb = logo;
			}
		}

		// Processes.
		await this.UpdateProcesses();

		// GPU info for NV
		this.UpdateGpuInfo();
	}

	public async Task UpdateProcesses()
	{
		HashSet<ProgramEntry> unvisited = new(this.Where(p => p.ProcessIds.Count > 0));

		// This is pretty slow, do it in thread pool to not lag the UI.
		(Process proc, string? path)[] processes = await Task.Run(() =>
		{
			return Process.GetProcesses().Select(proc =>
			{
				string? path;
				try
				{
					path = proc.MainModule?.FileName;
				}
				catch (InvalidOperationException)
				{
					path = null;
				}
				catch (Win32Exception)
				{
					path = null;
				}

				return (proc, path);
			}).ToArray();
		});

		// Processes
		foreach ((Process process, string? path) in processes)
		{
			if (path == null)
				continue;

			ProgramEntry? entry = this.FirstOrDefault(e => e.Path == path);
			if (entry != null)
			{
				entry.Kind = ProgramKind.Exe;
			}
			else
			{
				entry = new ProgramEntry
				{
					Path = path,
				};
				this.Add(entry);
			}

			unvisited.Remove(entry);

			if (entry.ProcessIds.Add(process.Id))
			{
				entry.OnPropertyChanged(nameof(entry.ProcessId));
			}
		}

		foreach (ProgramEntry entry in unvisited)
		{
			entry.ProcessIds.Clear();
			entry.OnPropertyChanged(nameof(entry.ProcessId));
		}
	}

	public void UpdateGpuInfo()
	{
		Dictionary<int, List<string>> processGpus = new();
		foreach (PhysicalGPU? gpu in PhysicalGPU.GetPhysicalGPUs())
		{
			foreach (PrivateActiveApplicationV2 app in GPUApi.QueryActiveApps(gpu.Handle))
			{
				if (!processGpus.TryGetValue(app.ProcessId, out List<string>? gpus))
					processGpus[app.ProcessId] = gpus = new();
				gpus.Add(gpu.FullName);
			}
		}

		HashSet<string> inUse = new();
		foreach (ProgramEntry entry in this)
		{
			inUse.Clear();

			foreach (int pid in entry.ProcessIds)
			{
				if (processGpus.TryGetValue(pid, out List<string>? gpus))
				{
					foreach (string gpu in gpus)
					{
						inUse.Add(gpu);
					}
				}
			}

			entry.GpuInfo = string.Join(", ", inUse.Select(APIs.ShortenGpuName));
		}
	}

	protected override void RemoveItem(int index)
	{
		ProgramEntry entry = this[index];
		entry.Preference = Preference.Unset;
		entry.OpenGLGpu = null;
		base.RemoveItem(index);
	}
}