﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace GpuPreference;

/// <summary>
/// Interaction logic for MainWindow.xaml
/// </summary>
public partial class MainWindow : Window
{
	private readonly DispatcherTimer timer;

	public ProgramList Programs { get; }

	public MainWindow()
	{
		this.Programs = new ProgramList();

		this.InitializeComponent();

		this.timer = new DispatcherTimer(TimeSpan.FromSeconds(1), DispatcherPriority.Normal, this.tick, Dispatcher.CurrentDispatcher);
		this.timer.Start();
	}

	protected override async void OnInitialized(EventArgs e)
	{
		base.OnInitialized(e);
		await this.Programs.LoadAsync();
	}

	protected override void OnClosed(EventArgs e)
	{
		base.OnClosed(e);
		this.timer.Stop();
	}

	private async void tick(object? sender, EventArgs e)
	{
		await this.Programs.UpdateProcesses();
		this.Programs.UpdateGpuInfo();
	}

	private async void refresh(object sender, ExecutedRoutedEventArgs e)
	{
		await this.Programs.LoadAsync();
	}

	private void addNew(object sender, ExecutedRoutedEventArgs e)
	{
		AddDialog d = new();
		if (d.ShowDialog() == true)
		{
			this.Programs.Insert(0, new ProgramEntry()
			{
				Path = d.Path,
			});
		}
	}
}