﻿using System.Windows;
using Microsoft.Win32;

namespace GpuPreference;

public partial class AddDialog : Window
{
	public static readonly DependencyProperty PathProperty = DependencyProperty.Register(
		nameof(Path), typeof(string), typeof(AddDialog), new PropertyMetadata(default(string)));

	public string Path
	{
		get => (string)this.GetValue(PathProperty);
		set => this.SetValue(PathProperty, value);
	}

	public AddDialog()
	{
		this.InitializeComponent();
	}

	private void onOkClick(object sender, RoutedEventArgs e)
	{
		this.DialogResult = true;
		this.Close();
	}

	private void onCancelClick(object sender, RoutedEventArgs e)
	{
		this.DialogResult = false;
		this.Close();
	}

	private void browse(object sender, RoutedEventArgs e)
	{
		OpenFileDialog d = new()
		{
			Filter = "Executables (*.exe)|*.exe|All Files|*.*"
		};
		if (d.ShowDialog() == true)
		{
			this.Path = d.FileName;
		}
	}
}