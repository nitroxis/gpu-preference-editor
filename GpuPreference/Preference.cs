﻿namespace GpuPreference;

public enum Preference
{
	Unset = -1,
	Auto = 0,
	PowerSave = 1,
	Performance = 2,
}