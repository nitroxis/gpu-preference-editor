﻿using System.Windows;

namespace GpuPreference;

/// <summary>
/// Interaction logic for App.xaml
/// </summary>
public partial class App : Application
{
	protected override void OnStartup(StartupEventArgs e)
	{
		base.OnStartup(e);
		NvAPIWrapper.NVIDIA.Initialize();
		Thumbnailer.Start().Wait();
	}

	protected override void OnExit(ExitEventArgs e)
	{
#pragma warning disable CS4014
		Thumbnailer.Stop();
#pragma warning restore CS4014

		base.OnExit(e);
	}
}