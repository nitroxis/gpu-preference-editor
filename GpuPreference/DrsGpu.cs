﻿namespace GpuPreference;

public record DrsGpu(string Name, string Id);